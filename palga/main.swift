//
//  main.swift
//  palga
//
//  Created by David van Enckevort on 12-08-17.
//  Copyright © 2017 David van Enckevort. All rights reserved.
//

import Foundation

struct Thesaurus: Codable {
    var data: [ThesaurusItem]
    //    var options: [AnyObject]
    //    var files: [AnyObject]
}

struct ThesaurusItem: Codable {
    var DT_RowId: String
    var DETEROM: String
    var DEPALCE: String
    var DESTACE: String
    var DEADVOM: String?
    var DESNOMEDCT: String?
    var DERETR: String?
}

struct RetrievalTerm {
    var label: String
    var code: Int
}
struct Code {
    var preferredTerm: String
    var code: String
    var retrievalTerms: [RetrievalTerm]
}

enum Gender: String {
    case Male = "m"
    case Female = "v"
    static var types: [Gender] {
        return [.Male, .Female]
    }
}

enum Examination: String {
    case Cytology = "C"
    case Histology = "T"
    static var types: [Examination] {
        return [.Cytology, .Histology]
    }
}

enum Age: String {
    case child = "<18 jaar"
    case adult = "18-50 jaar"
    case elderly = ">50 jaar"
    static var groups: [Age] {
        return [.child, .adult, .elderly]
    }
}

struct ExcerptLine {
    var number: Int
    var code: [Code]
    var examination: Examination
}

struct Excerpt {
    var number: Int
    var year: Int
    var gender: Gender
    var age: Age
    var lines: [ExcerptLine]
}

struct HtmlFragment {
    var tag: String?
    var attributes: [String:String]?
    var body: String?
    var children: [HtmlFragment]?
}

class RetrievalTermParser: NSObject, XMLParserDelegate {
    private var currentFragment: HtmlFragment?
    private var fragments = [HtmlFragment]()
    private static let wrapper = "wrapper"
    private var parser: XMLParser?

    func values() -> [RetrievalTerm] {
        return fragments.map { return term(from: $0)}
    }

    func parse(terms: String) -> Bool {
        currentFragment = nil
        fragments = []
        let parser = XMLParser(data: terms.data(using: .utf8)!)
        parser.delegate = self
        return parser.parse()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        guard elementName != RetrievalTermParser.wrapper else { return }
        currentFragment = HtmlFragment()
        currentFragment?.tag = elementName
        currentFragment?.attributes = attributeDict
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        guard elementName != RetrievalTermParser.wrapper else { return }
        fragments.append(currentFragment!)
        currentFragment = nil
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if currentFragment?.body == nil {
            currentFragment?.body = string
        } else {
            currentFragment?.body?.append(string)
        }
    }

    private func term(from fragment: HtmlFragment) -> RetrievalTerm {
        let body = fragment.body!
        let code = Int(body)!
        let title = fragment.attributes!["title"]!
        return RetrievalTerm(label: title, code: code)
    }

}

struct Palga {
    private var palgaCodes: [Code]
    private let header = "PALGAexcerptnr, Regelnummer, PALGA-diagnose, PALGA-code, Retrievalterm, Soort onderzoek, Jaar onderzoek, Geslacht, Leeftijdscategorie\n".data(using: .utf8)!

    init(url: URL) throws {
        let contents = try Data(contentsOf: url)
        let parser = RetrievalTermParser()
        palgaCodes = [Code]()
        let decoder = JSONDecoder()
        let thesaurus = try decoder.decode(Thesaurus.self, from: contents)
        for item in thesaurus.data {
            var retrievalTerms = [RetrievalTerm]()
            if let text = item.DERETR, parser.parse(terms: text) {
                retrievalTerms = parser.values()
            }
            let code = Code(preferredTerm: item.DETEROM, code: item.DEPALCE, retrievalTerms: retrievalTerms)
            palgaCodes.append(code)
        }
    }

    init(file: String) throws {
        let parser = RetrievalTermParser()
        palgaCodes = [Code]()
        let handle = FileHandle(forReadingAtPath: file)
        defer { handle?.closeFile() }
        guard let contents = handle?.readDataToEndOfFile() else {
            throw PalgaError.FileError
        }
        let decoder = JSONDecoder()
        let thesaurus = try decoder.decode(Thesaurus.self, from: contents)
        for item in thesaurus.data {
            var retrievalTerms = [RetrievalTerm]()
            if let text = item.DERETR, parser.parse(terms: text) {
                retrievalTerms = parser.values()
            }
            let code = Code(preferredTerm: item.DETEROM, code: item.DEPALCE, retrievalTerms: retrievalTerms)
            palgaCodes.append(code)
        }
    }

    private func random<T>(_ array: [T]) -> T {
        let index = Int(arc4random_uniform(UInt32(array.count)))
        return array[index]
    }

    private func prepare(file: String) -> FileHandle? {
        var handle: FileHandle?
        if FileManager.default.fileExists(atPath: file) {
            handle = FileHandle(forUpdatingAtPath: file)
            handle?.seekToEndOfFile()
        } else {
            FileManager.default.createFile(atPath: file, contents: nil, attributes: [:])
            handle = FileHandle(forWritingAtPath: file)
            handle?.write(header)
        }
        return handle
    }

    func generate(count: Int, start: Int) {
        let end = start + count - 1
        let file = "palga-\(start)-\(end).csv"

        var handle = prepare(file: file)
        defer { handle?.closeFile() }

        var buffer = Data()
        for number in start...end {
            let excerpt = make(excerpt: number)
            buffer.append(excerpt)
        }
        handle?.write(buffer)
    }

    private func make(excerpt: Int) -> Excerpt {
        let years = (1991...2017).map { return $0 }
        var lines = [ExcerptLine]()
        let count = arc4random_uniform(UInt32(5)) + 1
        repeat {
            lines.append(make(line: lines.count))
        } while lines.count < count

        return Excerpt(number: excerpt, year: random(years), gender: random(Gender.types), age: random(Age.groups), lines: lines)
    }

    private func make(line: Int) -> ExcerptLine {
        let count = arc4random_uniform(5) + 1
        var codes = [Code]()
        repeat {
            codes.append(random(palgaCodes))
        } while codes.count < count
        return ExcerptLine(number: line, code: codes, examination: random(Examination.types))
    }

    enum PalgaError: Error {
        case FileError
        case ParseError
    }
}

extension Data {
    mutating func append(_ excerpt: Excerpt) {
        for line in excerpt.lines {
            let diagnosis = line.code.map { return $0.preferredTerm}.joined(separator: "*")
            let codes = line.code.map { return $0.code }.joined(separator: "*")
            let terms = line.code.map { return $0.retrievalTerms.map { return "\($0.code)" }.joined(separator: "*") }.joined(separator: "*")
            let line = "\(excerpt.number), \(line.number), \(diagnosis), \(codes), \(terms), \(line.examination.rawValue), \(excerpt.year), \(excerpt.gender.rawValue), \(excerpt.age.rawValue)\n"
            let data = line.data(using: .utf8)!
            self.append(data)
        }
    }
}
struct Atomic<T: Numeric> {
    var value: T = 0
    var semaphore = DispatchSemaphore(value: 1)

    mutating func incrementAndGet() -> T {
        semaphore.wait()
        defer { semaphore.signal() }
        value += 1
        return value
    }

    mutating func getAndIncrement() -> T {
        semaphore.wait()
        defer { semaphore.signal() }
        value += 1
        return value - 1
    }

    mutating func increment() {
        semaphore.wait()
        defer { semaphore.signal() }
        value += 1
    }

    func get() -> T {
        semaphore.wait()
        defer { semaphore.signal() }
        return value
    }
}

//let count = 36_478_638
let count = 10_000_000
let step = 10_000
//let thesaurus = "/Users/david/Developer/palga/palga-thesaurus.json"
let thesaurus = URL(string: "https://www.palga.nl/thesaurus/php/table.palga.php")!
//let out = "/Users/david/palga.csv"
do {
    let palga = try Palga(url: thesaurus)
    let createQueue = DispatchQueue(label: "creator", qos: .background, attributes: .concurrent, autoreleaseFrequency: .inherit, target: DispatchQueue.global(qos: .utility))
    let group = DispatchGroup()
    var counter = Atomic<Int>()
    for _ in stride(from: 0, to: count, by: step) {
        createQueue.async(group: group) {
            var start = counter.getAndIncrement()
            start *= step
            palga.generate(count: step, start: start)
            print(".", separator: "", terminator: "")
        }
    }
    group.wait()
} catch {
    fatalError("Failed to initialize: \(error)")
}
